# fixed-lru: a fixed size LRU library for rust

This implements a simple LRU library in rust that uses a single fixed-size allocation
of memory for the cache. After creation, the cache itself does not make any allocations.

## Goals

This project has the following primary goals:

1. No unsafe code. `#![forbid(unsafe_code)]` is enabled, and will remain so.
2. No allocations after creation. Unlike a normal HashMap, it doesn't resize as
   elements are added.

While ideally it should also be fast, I won't sacrifice giving up on using only safe code to
get there for this project.

## Implementation

This LRU cache is implemented using a hash table with a fixed capacity using a simple robin-hood
algorithm with linear probing. Each entry is also a node in a doubly linked list to track how
recently an item was used. The "pointers" are actuallly indices into the array backing the hash
table. When a slot is stolen during insertion due to the robin hood algorithm, pointers must
be updated.

## Motivation

After reading a [blog post about implementing an LRU in rust](https://dev.to/seanchen1991/implementing-an-lru-cache-in-rust-33pp)
I came up with an idea of how it might be possible to combine a hash-table and doubly-linked
list in a single data structure that could be used as an LRU cache. And since an LRU cache
has a fixed size you don't need to worry about rebalancing.


