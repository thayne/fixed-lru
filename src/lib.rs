#![forbid(unsafe_code)]

use std::borrow::Borrow;
use std::collections::hash_map::RandomState;
use std::hash::{BuildHasher, Hash, Hasher};
use std::mem;

const END: usize = usize::MAX;

#[derive(Debug)]
pub struct LruCache<K, V, S = RandomState> {
    items: Box<[Entry<K, V>]>,
    newest: usize,
    oldest: usize,
    count: usize,
    max: usize,
    hasher: S,
}

#[derive(Copy, Clone, Debug)]
struct Links {
    newer: usize,
    older: usize,
}

impl Links {
    const DEFAULT: Self = Links {
        newer: END,
        older: END,
    };
}

#[derive(Debug)]
struct Entry<K, V> {
    item: Option<(K, V)>,
    hash: u64, // should this be smaller
    probe_len: u32,
    links: Links,
}

impl<K, V, S> LruCache<K, V, S> {
    pub fn new(size: usize, buffer: usize, hasher: S) -> Self {
        assert!(
            buffer > 0 && size > 0,
            "size and buffer must both be greater than zero"
        );
        let cap = size + buffer;
        let mut items = Vec::with_capacity(cap);
        // is there a better way to do this?
        for _ in 0..cap {
            items.push(Entry::default());
        }
        Self {
            items: items.into_boxed_slice(),
            newest: END,
            oldest: END,
            count: 0,
            max: size,
            hasher,
        }
    }

    pub fn with_size_and_hasher(size: usize, hasher: S) -> Self {
        let buffer = size / 4 + 1; // Default to a max load of ~ .8
        Self::new(size, buffer, hasher)
    }

    pub fn len(&self) -> usize {
        self.count
    }

    pub fn is_empty(&self) -> bool {
        self.count == 0
    }

    /// Return an iterator that iterates over the entries in the cache
    /// from most recently used to the least recently used.
    pub fn iter(&self) -> Iter<'_, K, V> {
        Iter {
            cache: &self.items,
            idx: self.newest,
        }
    }
}

impl<K, V> LruCache<K, V, RandomState> {
    pub fn with_size_and_buffer(size: usize, buffer: usize) -> Self {
        LruCache::new(size, buffer, RandomState::new())
    }

    pub fn with_size(size: usize) -> Self {
        LruCache::with_size_and_hasher(size, RandomState::new())
    }
}

impl<K, V, S> LruCache<K, V, S>
where
    K: Eq + Hash,
    S: BuildHasher,
{
    pub fn insert(&mut self, key: K, value: V) -> Option<V> {
        let hash = self.hash_key(&key);
        let mut current = Entry {
            item: Some((key, value)),
            hash,
            probe_len: 0,
            links: Links::DEFAULT,
        };
        let key = &current.item.as_ref().unwrap().0;
        for idx in self.buckets(hash) {
            let entry = &mut self.items[idx];
            if entry.item.is_none() {
                // empty slot, just insert
                *entry = current;
                self.set_newest(idx);
                self.increment_count();
                return None;
            }
            if hash == entry.hash && entry.valid_for_key(key) {
                // put the new value in the table
                let old = mem::replace(entry, current);
                // reconnect the broken links
                self.relink(old.links);
                // put this one at the front of the list.
                self.set_newest(idx);
                // Return the old value
                return old.item.map(|i| i.1);
            }
            if current.probe_len > entry.probe_len {
                current = mem::replace(entry, current);
                self.relink(current.links);
                // we no longer need to check for a matching entry, so just find where to
                // put the old node using the robin hood algorithm
                self.steal(idx, current);
                self.set_newest(idx);
                self.increment_count();
                return None;
            }

            current.probe_len += 1;
        }
        unreachable!();
    }

    pub fn get<Q: ?Sized>(&mut self, k: &Q) -> Option<&mut V>
    where
        K: Borrow<Q>,
        Q: Hash + Eq,
    {
        self.get_idx(k)
            .map(move |idx| &mut self.items[idx].item.as_mut().unwrap().1)
    }

    pub fn get_or_insert<F>(&mut self, k: K, f: F) -> &mut V
    where
        F: FnOnce() -> V,
    {
        let hash = self.hash_key(&k);
        let mut final_index = 0;
        let mut inserted = false;
        let new_entry = move |k, probe_len| Entry {
            item: Some((k, f())),
            hash,
            probe_len,
            links: Links::DEFAULT,
        };
        for (probe_len, idx) in self.buckets(hash).enumerate() {
            let entry = &mut self.items[idx];
            if entry.item.is_none() {
                // empty slot, so insert
                *entry = new_entry(k, probe_len as u32);
                inserted = true;
                final_index = idx;
                break;
            }
            if hash == entry.hash && entry.valid_for_key(&k) {
                final_index = idx;
                let links = entry.links;
                self.relink(links);
                break;
            }
            if probe_len > entry.probe_len as usize {
                let old = mem::replace(entry, new_entry(k, probe_len as u32));
                self.relink(old.links);
                self.steal(idx, old);
                inserted = true;
                final_index = idx;
                break;
            }
        }
        self.set_newest(final_index);
        if inserted {
            self.increment_count();
        }
        return &mut self.items[final_index].item.as_mut().unwrap().1;
    }

    fn get_idx<Q: ?Sized>(&mut self, k: &Q) -> Option<usize>
    where
        K: Borrow<Q>,
        Q: Hash + Eq,
    {
        let hash = self.hash_key(k);
        for (probe, idx) in self.buckets(hash).enumerate() {
            let entry = &self.items[idx];
            if hash == entry.hash && entry.valid_for_key(k) {
                let links = entry.links;
                self.relink(links);
                self.set_newest(idx);
                return Some(idx);
            }
            if entry.item.is_none() || probe > entry.probe_len as usize {
                // We didn't find any matches
                return None;
            }
        }
        unreachable!();
    }

    fn hash_key<Q: Hash + ?Sized>(&self, key: &Q) -> u64 {
        let mut hasher = self.hasher.build_hasher();
        key.hash(&mut hasher);
        hasher.finish()
    }

    /// Increment the count of items, if we exceed the capacity, prune the oldest
    /// item.
    fn increment_count(&mut self) {
        self.count += 1;
        while self.count > self.max {
            self.delete_oldest();
        }
    }

    fn steal(&mut self, mut idx: usize, mut current: Entry<K, V>) {
        let length = self.items.len();
        let mut links = current.links;

        loop {
            // idx is the original location so start by moving forward
            idx = (idx + 1) % length;
            current.probe_len += 1;

            let entry = &mut self.items[idx];
            if entry.item.is_none() {
                // found an empty slot
                *entry = current;
                self.update_neighbors(links, idx);
                return;
            }
            if current.probe_len > entry.probe_len {
                current = mem::replace(entry, current);
                self.update_neighbors(links, idx);
                links = current.links;
            }
        }
    }

    fn delete_oldest(&mut self) {
        let idx = self.oldest;
        let deleted = &mut self.items[idx];
        deleted.item = None;
        deleted.hash = 0;
        let oldest = deleted.links.newer;
        self.update_older(oldest, END);
        self.oldest = oldest;
        self.count -= 1;
        self.post_delete(idx);
    }

    /// Move items closer to hash bucket if posible
    /// after a deletion
    fn post_delete(&mut self, mut idx: usize) {
        let len = self.items.len();
        loop {
            let next = (idx + 1) % len;
            if self.items[next].is_displaced() {
                self.items.swap(idx, next);
                self.items[idx].probe_len -= 1;
                idx = next;
            } else {
                return;
            }
        }
    }

    fn update_older(&mut self, target: usize, older: usize) {
        if target == END {
            self.newest = older;
        } else {
            self.items[target].links.older = older;
        }
    }

    fn update_newer(&mut self, target: usize, newer: usize) {
        if target == END {
            self.oldest = newer;
        } else {
            self.items[target].links.newer = newer;
        }
    }

    fn update_neighbors(&mut self, links: Links, idx: usize) {
        self.update_older(links.older, idx);
        self.update_newer(links.newer, idx);
    }

    fn set_newest(&mut self, idx: usize) {
        let newest = mem::replace(&mut self.newest, idx);
        self.items[idx].links.older = newest;
        if newest != END {
            self.items[newest].links.newer = idx;
        } else {
            // if newest was END, then the cache shoudl be empty, and
            // we need to set this as the oldest too.
            self.oldest = idx;
        }
    }

    fn relink(&mut self, links: Links) {
        self.update_older(links.newer, links.older);
        self.update_newer(links.older, links.newer);
    }

    fn buckets(&self, hash: u64) -> Buckets {
        let length = self.items.len();
        let idx = hash as usize % length;
        Buckets { idx, length }
    }
}

impl<K, V> Default for Entry<K, V> {
    fn default() -> Self {
        Self {
            item: None,
            hash: 0,
            probe_len: u32::MAX,
            links: Links::DEFAULT,
        }
    }
}

impl<K, V> Entry<K, V> {
    fn valid_for_key<Q>(&self, q: &Q) -> bool
    where
        K: Borrow<Q>,
        Q: Eq + ?Sized,
    {
        if let Some((ref k, _)) = self.item {
            k.borrow() == q
        } else {
            // if the entry is empty, then we can use it to store a new item in.
            true
        }
    }

    // check if the entry can be moved closer to its ideal hash bucket
    fn is_displaced(&self) -> bool {
        self.item.is_some() && self.probe_len > 0
    }
}

struct Buckets {
    idx: usize,
    length: usize,
}

impl Iterator for Buckets {
    type Item = usize;

    fn next(&mut self) -> Option<usize> {
        let n = self.idx;
        self.idx = (n + 1) % self.length;
        Some(n)
    }
}

pub struct Iter<'a, K, V> {
    cache: &'a [Entry<K, V>],
    idx: usize,
}

impl<'a, K, V> Iterator for Iter<'a, K, V>
where
    K: std::fmt::Debug,
    V: std::fmt::Debug,
{
    type Item = (&'a K, &'a V);

    fn next(&mut self) -> Option<Self::Item> {
        let idx = self.idx;
        if idx == END {
            None
        } else {
            let entry = &self.cache[idx];
            self.idx = entry.links.older;
            return entry.item.as_ref().map(|(ref k, ref v)| (k, v));
        }
    }

    // TODO: implement size_hint and count?
}

#[cfg(test)]
mod test {
    use super::LruCache;

    fn compare_cache<'a, K, V, I>(cache: &LruCache<K, V>, expected: I) -> bool
    where
        I: Iterator<Item = &'a (K, V)> + 'a,
        K: std::cmp::PartialEq + 'a + std::fmt::Debug,
        V: std::cmp::PartialEq + 'a + std::fmt::Debug,
    {
        expected.map(|(k, v)| (k, v)).eq(cache.iter())
    }

    macro_rules! assert_contains {
        ($cache:expr, $expected:expr) => {
            let cache = &$cache;
            let expected = $expected;
            assert!(
                compare_cache(cache, expected.iter()),
                "Expected {:?} in cache but got {:?}",
                expected,
                cache.iter().collect::<Vec<_>>()
            );
        };
    }

    #[test]
    fn insert() {
        let mut cache = LruCache::<i32, &'static str>::with_size(4);
        cache.insert(3, "hello");
        assert_eq!(1, cache.len());
        cache.insert(4, "bye");
        assert_contains!(cache, [(4, "bye"), (3, "hello")]);
        assert_eq!(2, cache.len());
    }

    #[test]
    fn move_up_on_get() {
        let mut cache = LruCache::<i32, i32>::with_size(5);
        cache.insert(1, 1);
        cache.insert(2, 4);
        cache.insert(3, 9);
        cache.insert(4, 16);

        assert_eq!(&4, cache.get(&2).unwrap());
        cache.get(&1);
        assert_contains!(cache, [(1, 1), (2, 4), (4, 16), (3, 9)]);
    }
}
